package fs;

import java.util.Map;


/**
 * 该程序的能力：
 * 一个戏剧演出团，演员们经常去各种场合演喜剧，通常（customer）会指定几出剧，而剧团根据观众（audience）人数以及剧目来向客户收费
 * 该剧团目前出演两种戏剧，悲剧（tragedy） 和戏剧（comedy），给客户发出账单时，剧团还会根据到场观众的数量给出“观众积分”（volume credit） 优惠
 * 下次客户再次看就可以有折扣。
 * 
 * 问题：
 * 1.该程序设计的如何？
 * 2.如果我要添加新功能比如我要添加新的剧目我该如何添加？
 * 3.你觉得该代码中有哪些辣眼睛的地方呢？
 * 4.对于问题3，如果你觉得不好，那如果时你你会如何实现？
 * 
 * @author benjun.wang
 *
 */
public class Statement {

	public Statement(Invoice invoice,Map<String,Play> plays) throws Exception {
		int totalAmount = 0;
		int volumeCrdits = 0;
		StringBuilder result = new StringBuilder();
		result.append("statement for ");
		result.append(invoice.customer);
		result.append("\n");
		
		for(Performance item :invoice.performances) {
			Play play = plays.get(item.playID);
			int thisAmount = 0;

			switch (play.type) {
			case "tragedy":
				thisAmount = 40000;
				if(item.audience > 30) {
					thisAmount += 1000 * (item.audience - 30);
				}
				break;
			case "comedy":
				thisAmount = 30000;
				if(item.audience > 20) {
					thisAmount += 1000 + 500 * (item.audience - 20);
				}
				thisAmount += 300 * item.audience;
				break;
			default:
				throw new Exception("unknown type " + play.type);
			}
			
//			add volume cedits
			volumeCrdits += Math.max(item.audience-30, 0);
//			add extra credit for every ten condy attendees
			if("comedy".equals(play.name)) {
				volumeCrdits += Math.floor(item.audience/5);
			}
			result.append(play.name + ":" + thisAmount/100 + "(" + item.audience + "seats)" );
			result.append("\n");
			totalAmount += thisAmount;
		}
		result.append("Amount owed is " + totalAmount/100);
		result.append("\n");
		result.append("You earned " + volumeCrdits + "credits");
		result.append("\n");
		
		System.out.println(result);
		
	}

}
